phoenix_docker
=====

some dockerfile & docker-compose for Elixir/Phoenix app

## Database note
don't forget to change config files in your phoenix app to retrieve env variable

# Prod Mod

To create your image 
```
docker build -t $YOUR_IMAGE_NAME . \
	--build-arg git_repo="https://linktoyour.repo" \
	--build-arg secret="your_secret_key_base" \
```

then change docker-compose.yml to
```
    image: $YOUR_IMAGE_NAME
```

then just launch with ```docker-compose up```


# Release Mod

Before build the images please gen config file for release with ```./gen_docker.sh my_app MyApp```

Edit - PORT / - DATABASE_URL in docker-compose.yml if needed !

launch the website with ```docker-compose up -d```
seed the website with ```docker-compose exec web /app/bin/my_app rpc ":code.priv_dir(:my_app) |> Path.join('repo/seeds.exs') |> Code.eval_file()"```

If you want to reset database, please just delete the volume.
```docker rm $(docker ps -q -a)```
```docker volume rm $(docker volume ls)```

To build you image, please run this command
```
docker build -t $YOUR_IMAGE_NAME . \
	--build-arg git_repo="https://linktoyour.repo" \
	--build-arg secret="your_secret_key_base" \
	--build-arg app_name="your_app_name" \
	```
